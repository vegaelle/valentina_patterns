#!/usr/bin/env python3

from xml.etree import ElementTree
import argparse
import os
from typing import TypedDict
from itertools import count


class Measurement(TypedDict):
    value: str
    duplicates: int
    element: ElementTree.Element


def main():
    parser = argparse.ArgumentParser(description="Edit a Valentina measurements file to"
                                     " remove duplicate values")
    parser.add_argument("file", type=argparse.FileType(), help="Measurements file")
    parser.add_argument("--new_name", "-n", help="New filename", default=None)
    parser.add_argument("--verbose", "-v", action="store_true", help="Verbose mode")

    args = parser.parse_args()

    args.file.close()
    doc = ElementTree.parse(args.file.name)
    root = doc.getroot()
    body_measurements = root.find("body-measurements")
    if body_measurements is None:
        raise Exception("Measurements tag not found, file could be corrupted")
    measurements: dict[str, Measurement] = {}
    removal_count = 0
    c = 0
    to_remove: list[ElementTree.Element] = []

    for c, m in enumerate(body_measurements, 1):
        if m.get("type") != "separator":
            m_name = m.attrib["name"]
            m_value = m.attrib["value"]
            if m_name in measurements:
                if args.verbose:
                    print(f"Found duplicate measurement of {m_name} (found "
                          f"{measurements[m_name]['duplicates']+1} time)")
                if m_value != "0" and m_value != measurements[m_name]["value"]:
                    if args.verbose:
                        print(f"Value is non-empty for {m_name}, value: '{m_value}',"
                              f"previous value: '{measurements[m_name]['value']}'")
                    if measurements[m_name]["value"] == "0":
                        measurements[m_name]["element"].attrib["value"] = m_value
                        if args.verbose:
                            print(f"Fixing original value for {m_name}")
                to_remove.append(m)
                removal_count += 1
            else:
                measurements[m_name] = {"value": m_value, "duplicates": 0, "element": m}

    for m in to_remove:
        body_measurements.remove(m)
    # breakpoint()
    # root.remove(body_measurements)
    # root.append(body_measurements)
    
    if args.verbose:
        print(f"Found {c} total measurements, {len(measurements)} unique measurements,"
              f"removed {removal_count} values.")

    if args.new_name:
        new_name = args.new_name
    else:
        name, ext = os.path.splitext(args.file.name)
        new_name = f"{name}_dedup{ext}"
        if os.path.exists(new_name):
            c = count(2)
            while os.path.exists(new_name):
                new_name = f"{name}_dedup_{next(c)}{ext}"
    ElementTree.indent(doc, space='    ')
    doc.write(new_name, encoding="UTF-8", xml_declaration=True)
    print(f"Written {len(measurements)} measurements in {new_name}")


if __name__ == "__main__":
    main()
